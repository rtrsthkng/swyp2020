# SWYP 2020 `git` workshop

This is a dummy project for the purpose of learning the basic aspects of `git`.

The workshop is to be held on the 2020 edition of the IEEE SWYP congress, in Zaragoza, Spain.

## How to proceed

First of all, download Git Bash in Windows.
